const webpack = require('webpack')
const path = require('path')

const config = {
  context: path.resolve(__dirname, 'src'),
  entry: {
    app: [
      'babel-polyfill',
      'react-hot-loader/patch',
      './index.js'
    ]
  },
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, './build'),
    publicPath: '/'
  },
  resolve: {
    alias: {
      Features: path.resolve(__dirname, 'src/features'),
      Config: path.resolve(__dirname, 'src/_config'),
      HOCS: path.resolve(__dirname, 'src/hocs'),
      Utils: path.resolve(__dirname, 'src/utils')
    }
  },
  devtool: 'inline-source-map',

  devServer: {
    inline: true,
    host: '0.0.0.0',
    port: 3000,
    historyApiFallback: true,
    disableHostCheck: true,
    contentBase: path.join(__dirname, 'public'),
    hot: true
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: [/node_modules/],
        use: {
          loader: 'babel-loader'
        }
      }
    ]
  },

  plugins: [
    new webpack.NamedModulesPlugin(),
    new webpack.HotModuleReplacementPlugin()
  ]
}

module.exports = config

import {
  LOAD_CHARITIES_REQUEST,
  LOAD_CHARITIES_SUCCESS
} from '../../types'

const initialState = {
  isLoading: false,
  items: []
}

export default (state = initialState, action) => {
  switch (action.type) {
    case LOAD_CHARITIES_REQUEST:
      return {
        isLoading: true,
        items: []
      }
    case LOAD_CHARITIES_SUCCESS:
      return {
        isLoading: false,
        items: action.payload
      }
    default:
      return state
  }
}

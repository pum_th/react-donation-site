import PropTypes from 'prop-types'
import React from 'react'
import styled from 'styled-components'

function CharityDonateForm (props) {
  const {
    charity,
    onClickClose,
    paymentForm: PaymentForm
  } = props
  return (
    <Overlay>
      <Wrapper>
        <PaymentForm charity={charity} />
        <CloseButton onClick={onClickClose}>X</CloseButton>
      </Wrapper>
    </Overlay>
  )
}

CharityDonateForm.propTypes = {
  paymentForm: PropTypes.func,
  onClickClose: PropTypes.func,
  charity: PropTypes.object
}

const Overlay = styled.div`
background: rgba(255, 255, 255, 0.9);

  @media (min-width: 769px) {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
  }

  @media (max-width: 768px) {
    position: fixed;
    width: 100%;
    height: 100vh;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
  }
`

const CloseButton = styled.button`
  color: #627180;
  width: 50px;
  height: 50px;
  font-size: 18px;
  font-weight: 700;
  position: absolute;
  top: 0;
  right: 0;
  border: none;
  background: transparent;
  outline: none;
  cursor: pointer;
  @media (max-width: 768px) {
    width: 75px;
    height: 75px;
    font-size: 24px;
  }
`

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100%;
`

export default CharityDonateForm

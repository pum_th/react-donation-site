import {
  LOAD_CHARITIES_REQUEST,
  LOAD_CHARITIES_SUCCESS,
  LOAD_CHARITIES_FAILURE
} from '../../types'

import { RSAA } from 'redux-api-middleware'

export function loadCharities () {
  return {
    [RSAA]: {
      endpoint: 'http://localhost:3001/charities',
      method: 'GET',
      types: [LOAD_CHARITIES_REQUEST, LOAD_CHARITIES_SUCCESS, LOAD_CHARITIES_FAILURE]
    }
  }
}

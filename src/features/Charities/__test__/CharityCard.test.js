/* eslint-disable no-undef */

import React from 'react'
import CharityCard from '../components/CharityCard'

const mockFn = jest.fn()

it('renders correctly', () => {
  const wrapper = shallow(
    <CharityCard
      handleClickOutside={mockFn}
      onClickDonate={mockFn}
      showForm={false}
      data={{}}
      donateForm={mockFn}
      ui={{
        button: mockFn
      }}
    />
  )
  expect(wrapper).toMatchSnapshot()
})

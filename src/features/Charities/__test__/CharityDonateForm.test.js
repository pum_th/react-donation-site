/* eslint-disable no-undef */

import React from 'react'
import CharityDonateForm from '../components/CharityDonateForm'

const mockFn = jest.fn()

it('renders correctly', () => {
  const wrapper = shallow(
    <CharityDonateForm
      paymentForm={mockFn}
      onClickClose={mockFn}
      charity={{}}
    />
  )
  expect(wrapper).toMatchSnapshot()
})

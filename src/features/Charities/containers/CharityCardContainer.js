import { Button } from 'Features/UI'
import { compose, withState, withHandlers, withProps } from 'recompose'
import onClickOutside from 'react-onclickoutside'

import CharityCard from '../components/CharityCard'
import CharityDonateForm from '../components/CharityDonateForm'
import { PaymentForm } from 'Features/PaymentForm'

export default compose(
  withProps(() => ({
    donateForm: CharityDonateForm,
    ui: {
      button: Button,
      paymentForm: PaymentForm
    }
  })),
  withState('showForm', 'setShowForm', false),
  withHandlers({
    onClickDonate: ({setShowForm}) => e => {
      setShowForm(true)
    },
    closeFormHandler: ({ setShowForm }) => e => {
      setShowForm(false)
    },
    handleClickOutside: ({ setShowForm }) => e => {
      setShowForm(false)
    }
  }),
  onClickOutside
)(CharityCard)

import { combineReducers } from 'redux'
import { thankMessageReducers } from 'Features/ThankMessage'

export default combineReducers({
  thankMessage: thankMessageReducers
})

import styled from 'styled-components'
import { style } from 'Config'

const primaryColor = style.color.primary

const Button = styled.div`
  padding: 3px 8px 2px;
  color: ${primaryColor};
  border: 2px solid ${primaryColor};
  text-align: center;
  font-size: 18px;
  display: inline-block;
  border-radius: 5px;
  cursor: pointer;
  transition: background 0.2s ease;

  &:hover {
    color: #fff;
    background: ${primaryColor};
  }
`

export default Button

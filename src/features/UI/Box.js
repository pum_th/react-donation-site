import styled from 'styled-components'

const Box = styled.div`
  max-width: 1250px;
  margin: 0 auto;
  padding: 0 25px;
`

export default Box

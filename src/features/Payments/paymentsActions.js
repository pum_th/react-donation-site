import {
  LOAD_PAYMENTS_REQUEST,
  LOAD_PAYMENTS_SUCCESS,
  LOAD_PAYMENTS_FAILURE,
  CREATE_PAYMENT_REQUEST,
  CREATE_PAYMENT_SUCCESS,
  CREATE_PAYMENT_FAILURE
} from '../../types'

import { RSAA } from 'redux-api-middleware'

export function createPayment ({id, amount, currency}) {
  return {
    [RSAA]: {
      endpoint: 'http://localhost:3001/payments',
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        charitiesId: id,
        amount: amount,
        currency: currency
      }),
      types: [CREATE_PAYMENT_REQUEST, CREATE_PAYMENT_SUCCESS, CREATE_PAYMENT_FAILURE]
    }
  }
}

export function loadPayments () {
  return {
    [RSAA]: {
      endpoint: 'http://localhost:3001/payments',
      method: 'GET',
      types: [LOAD_PAYMENTS_REQUEST, LOAD_PAYMENTS_SUCCESS, LOAD_PAYMENTS_FAILURE]
    }
  }
}

import {
  LOAD_PAYMENTS_REQUEST,
  LOAD_PAYMENTS_SUCCESS,
  CREATE_PAYMENT_SUCCESS
} from '../../types'

const initialState = {
  isLoading: false,
  allPayments: []
}

export default (state = initialState, action) => {
  switch (action.type) {
    case LOAD_PAYMENTS_REQUEST:
      return {
        isLoading: true,
        allPayments: []
      }
    case LOAD_PAYMENTS_SUCCESS:
      return {
        isLoading: false,
        allPayments: action.payload
      }
    case CREATE_PAYMENT_SUCCESS:
      return {
        isLoading: false,
        allPayments: [...state.allPayments, action.payload]
      }
    default:
      return state
  }
}

import {
  UPDATE_THANK_MESSAGE
} from '../../types'

export function updateThankMessage (value) {
  return {
    type: UPDATE_THANK_MESSAGE,
    payload: value
  }
}

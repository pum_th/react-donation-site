import React from 'react'
import PropTypes from 'prop-types'
import { Box } from 'Features/UI'
import { compose } from 'recompose'
import { connect } from 'react-redux'
import styled from 'styled-components'
import { get } from 'lodash'

function TotalDonationBar ({show, charityName, amount, currency}) {
  return (
    <Bar show={show}>
      <Box>
        <TextGroup>
          <SmallText>Thank you for donating <strong>{amount}</strong> {currency}</SmallText>
          <Text>to {charityName}</Text>
        </TextGroup>
      </Box>
    </Bar>
  )
}

TotalDonationBar.propTypes = {
  show: PropTypes.bool,
  charityName: PropTypes.string,
  amount: PropTypes.number,
  currency: PropTypes.string
}

const Bar = styled.div`
  padding: 15px 25px;
  background: #FED175;
  color: #2C3D4D;
  width: 100%;
  position: fixed;
  top: 0;
  left: 0;
  transition: transform 0.6s ease;
  transform: translateY(${({show}) => show ? '0' : '-110px'});
`
const TextGroup = styled.div`
  text-align: center;
  margin: 0 auto;
  line-height: 1.2;
`
const Text = styled.h2`
  font-size: 30px;
  font-weight: 700;

  @media (max-width: 768px) {
    font-size: 24px;
  }
`
const SmallText = styled.span`
  text-align: center;
  font-size: 18px;
`

export default compose(
  connect(
    ({ui: {thankMessage}}) => ({
      show: get(thankMessage, 'show', false),
      charityName: get(thankMessage, 'charityName', null),
      amount: get(thankMessage, 'amount', null),
      currency: get(thankMessage, 'currency', null)
    })
  )
)(TotalDonationBar)

import {
  UPDATE_THANK_MESSAGE
} from '../../types'

const initialState = {}

export default (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_THANK_MESSAGE:
      return action.payload
    default:
      return state
  }
}

import React from 'react'
import styled from 'styled-components'

import { TotalDonationBar } from 'Features/TotalDonation'
import { ThankMessage } from 'Features/ThankMessage'

const Container = styled.div`
  height: 100px;
`

function Footer () {
  return (
    <Container>
      <TotalDonationBar />
      <ThankMessage />
    </Container>
  )
}

export default Footer

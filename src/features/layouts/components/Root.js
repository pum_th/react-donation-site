import React from 'react'
import App from './App'

function RootContainer () {
  return (
    <App />
  )
}

export default RootContainer

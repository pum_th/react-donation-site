import React from 'react'
import { compose } from 'recompose'
import { Provider } from 'react-redux'

import Header from 'Features/Layouts/components/Header'
import Footer from 'Features/Layouts/components/Footer'
import { CharityList } from 'Features/Charities'
import { Box } from 'Features/UI'

import { withGlobalStyle } from 'HOCS'

import { configureStore } from '../../../store'
const store = configureStore()

function App () {
  return (
    <Provider store={store}>
      <div>
        <Header />
        <Box>
          <CharityList />
        </Box>
        <Footer />
      </div>
    </Provider>
  )
}

export default compose(
  withGlobalStyle
)(App)

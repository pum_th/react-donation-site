import PropTypes from 'prop-types'
import React from 'react'
import styled from 'styled-components'

function CharityCard (props) {
  const {
    showForm,
    onClickDonate,
    closeFormHandler,
    data,
    donateForm: CharityDonateForm,
    data: {
      name,
      image
    },
    ui: {
      button: Button,
      paymentForm
    }
  } = props
  return (
    <Card>
      <Thumbnail>
        <img src={`images/${image}`} />
        {showForm &&
          <CharityDonateForm
            paymentForm={paymentForm}
            charity={data}
            onClickClose={closeFormHandler}
          />
        }
      </Thumbnail>
      <Detail>
        <Title>{name}</Title>
        <Button onClick={onClickDonate}>Donate</Button>
      </Detail>
    </Card>
  )
}

CharityCard.propTypes = {
  closeFormHandler: PropTypes.func,
  onClickDonate: PropTypes.func,
  showForm: PropTypes.bool,
  data: PropTypes.object,
  donateForm: PropTypes.func,
  ui: PropTypes.shape({
    button: PropTypes.func
  })
}

const Card = styled.div`
  border-radius: 10px;
  box-shadow: 2px 5px 15px #ddd;
  min-height: 150px;
  position: relative;

  @media (max-width: 768px) {
    position: initial;
  }

`

const Thumbnail = styled.div`
  font-size: 0;
  background: #222;
  border-top-right-radius: 10px;
  border-top-left-radius: 10px;

  img {
    width: 100%;
    border-top-right-radius: 10px;
    border-top-left-radius: 10px;
  }

`

const Detail = styled.div`
  padding: 25px;
  display: flex;
  justify-content: space-between;

  @media (max-width: 960px) {
    flex-direction: column;
  }
`

const Title = styled.div`
  font-size: 20px;
  font-weight: 500;

  @media (max-width: 960px) {
    text-align: center;
    margin-bottom: 10px;
  }
`

export default CharityCard

import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { connect } from 'react-redux'
import { compose, lifecycle } from 'recompose'

import { loadCharities } from '../charitiesActions'
import CharityCard from '../containers/CharityCardContainer'

function CharityList ({isLoading, charities}) {
  if (isLoading) return <div>Loading</div>

  return (
    <Row>
      {charities.map((charity, i) => (
        <Col key={`CharityCard_${i}`}>
          <CharityCard data={charity} />
        </Col>
      ))}
    </Row>
  )
}

CharityList.propTypes = {
  isLoading: PropTypes.bool,
  charities: PropTypes.array.isRequired
}

const Row = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin-left: -25px;
  margin-right: -25px;

  @media (max-width: 768px) {
    margin-left: -15px;
    margin-right: -15px;
  }

`

const Col = styled.div`
  flex: 1 0 50%;
  max-width: 50%;
  padding: 0 25px;
  margin-bottom: 50px;

  @media (max-width: 768px) {
    padding: 0 15px;
    margin-bottom: 30px;
  }

  @media (max-width: 640px) {
    flex: 1 0 100%;
    max-width: 100%;
  }

`

export default compose(
  connect(
    ({ charities }) => ({
      charities: charities.items,
      isLoading: charities.isLoading
    }),
    { loadCharities }
  ),
  lifecycle({
    componentDidMount () {
      this.props.loadCharities()
    }
  })
)(CharityList)

import {
  UPDATE_TOTAL_DONATION
} from '../../types'

export function updateTotalDonation (value) {
  return {
    type: UPDATE_TOTAL_DONATION,
    payload: value
  }
}

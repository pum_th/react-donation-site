import {
  UPDATE_TOTAL_DONATION
} from '../../types'

const initialState = 0

export default (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_TOTAL_DONATION:
      return action.payload
    default:
      return state
  }
}

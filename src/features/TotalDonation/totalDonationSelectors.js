import { createSelector } from 'reselect'
import { get, isEmpty } from 'lodash'
import { numberFormat } from 'Utils'

const summaryDonations = (paymentAmount) => (
  paymentAmount.reduce((accumulator, value) => (accumulator + value))
)

export const getTotalDonation = createSelector(
  state => state.payments,
  payments => {
    const paymentAmounts = payments.allPayments.map(payment => +(get(payment, 'amount', 0)))
    return isEmpty(paymentAmounts) ? 0 : numberFormat(summaryDonations(paymentAmounts))
  }
)

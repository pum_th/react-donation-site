import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { compose, lifecycle } from 'recompose'
import { connect } from 'react-redux'

import { loadPayments } from 'Features/Payments/paymentsActions'
import { updateTotalDonation } from '../totalDonationActions'
import { getTotalDonation } from '../totalDonationSelectors'
import { Box } from 'Features/UI'

function TotalDonationBar ({totalDonation}) {
  return (
    <Bar>
      <Box>
        <TextGroup>
          <Text>
            {totalDonation} THB
          </Text>
          <SmallText>All donations</SmallText>
        </TextGroup>
      </Box>
    </Bar>
  )
}

TotalDonationBar.propTypes = {
  totalDonation: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ])
}

const Bar = styled.div`
  padding: 15px 25px;
  background: #2C3D4D;
  color: #fff;
  width: 100%;
  position: fixed;
  bottom: 0;
  left: 0;
`
const TextGroup = styled.div`
  text-align: center;
  margin: 0 auto;
`
const Text = styled.h2`
  font-size: 30px;
  font-weight: 700;
`
const SmallText = styled.span`
  text-align: center;
  font-size: 18px;
`

export default compose(
  connect(
    (state) => ({
      totalDonation: getTotalDonation(state)
    }),
    {loadPayments, updateTotalDonation}
  ),
  lifecycle({
    componentDidMount () {
      this.props.loadPayments()
      this.props.updateTotalDonation(this.props.totalDonation)
    },
    componentWillReceiveProps (NextProps) {
      if (this.props.totalDonation !== NextProps.totalDonation) {
        this.props.updateTotalDonation(NextProps.totalDonation)
      }
    }
  })
)(TotalDonationBar)

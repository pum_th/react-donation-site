import PropTypes from 'prop-types'
import React from 'react'
import styled from 'styled-components'
import { connect } from 'react-redux'
import { compose, withState, withHandlers } from 'recompose'

import { createPayment } from 'Features/Payments/paymentsActions'
import { updateThankMessage } from 'Features/ThankMessage/thankMessageActions'

import { Button } from 'Features/UI'

function PaymentForm (props) {
  const {
    amount,
    onClickInput,
    onClickPay,
    charity: {
      id,
      name,
      currency
    }
  } = props

  return (
    <Group>
      <Title>
        <span>Select the amount to donate name ({currency})</span>
        <CharityName>for {name}</CharityName>
      </Title>
      <PaymentList>
        {
          [10, 20, 50, 100, 500].map((amount, index) => (
            <label key={index}>
              <input type="radio" name="payment" onClick={onClickInput(amount)} /> {amount}
            </label>
          ))
        }
      </PaymentList>
      <Button onClick={onClickPay({id, amount, currency})}>Pay</Button>
    </Group>
  )
}

PaymentForm.propTypes = {
  amount: PropTypes.number,
  charity: PropTypes.object,
  onClickInput: PropTypes.func,
  onClickPay: PropTypes.func
}

const Title = styled.h2`
  font-size: 20px;
  margin-bottom: 10px;
  padding: 0 30px;
  @media (max-width: 768px) {
    margin-bottom: 20px;
  }
`

const CharityName = styled.strong`
  display:none;
  font-size: 24px;
  @media (max-width: 768px) {
    display: block;
  }
`

const Group = styled.div`
  text-align: center;

  label {
    font-size: 18px;
    display: inline-block;
    margin-right: 14px;
    line-height: 0.8;
    cursor: pointer;

    &:hover {
      color: #222;
    }
  }

  @media (max-width: 768px) {
    label {
      margin-bottom: 5px;
    }
  }

`
const PaymentList = styled.div`
  display: block;
  margin-bottom: 15px;
`

export default compose(
  connect(),
  withState('amount', 'setAmount', 0),
  withHandlers({
    onClickInput: ({setAmount}) => n => e => {
      setAmount(n)
    },
    onClickPay: ({dispatch, charity, totalDonation}) => value => e => {
      const thankMsg = {
        amount: value.amount,
        charityName: charity.name,
        currency: charity.currency
      }

      dispatch(createPayment(value))
      dispatch(updateThankMessage({
        show: true,
        ...thankMsg
      }))

      setTimeout(function () {
        dispatch(updateThankMessage({
          show: false,
          ...thankMsg
        }))
      }, 1500)
    }
  })
)(PaymentForm)

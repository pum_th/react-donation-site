import { combineReducers } from 'redux'
import { charitiesReducer } from 'Features/Charities'
import { paymentsReducers } from 'Features/Payments'
import { totalDonationReducers } from 'Features/TotalDonation'
import { uiReducers } from 'Features/UI'

export default combineReducers({
  charities: charitiesReducer,
  payments: paymentsReducers,
  totalDonation: totalDonationReducers,
  ui: uiReducers
})

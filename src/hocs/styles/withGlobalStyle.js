import React from 'react'
import { injectGlobal } from 'styled-components'

import resetCSS from './resetCSS'
import baseCSS from './baseCSS'

const GlobalStyle = () => injectGlobal`
  ${resetCSS}
  ${baseCSS}
`

export default WrappedComponent => (props) => {
  GlobalStyle()
  return (
    <WrappedComponent {...props} />
  )
}

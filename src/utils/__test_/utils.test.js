/* eslint-disable no-undef */

import numberFormat from '../numberFormat'

describe('numberFormat', function () {
  test('`numberFormat` should grouped thousands correctly', function () {
    expect(numberFormat(2405)).toEqual('2,405')
  })
  test('`numberFormat` should grouped thousands correctly', function () {
    expect(numberFormat(16546435)).toEqual('16,546,435')
  })
  test('`numberFormat` should grouped thousands correctly', function () {
    expect(numberFormat(765785645643)).toEqual('765,785,645,643')
  })
})

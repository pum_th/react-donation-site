// Make Enzyme functions available in all test files without importing
import { shallow, render, mount } from 'enzyme'
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

global.XMLHttpRequest = require('w3c-xmlhttprequest').XMLHttpRequest
global.shallow = shallow
global.render = render
global.mount = mount

configure({ adapter: new Adapter() });

// Fail tests on any warning
console.error = message => {
  throw new Error(message)
}
